import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { es_ES } from 'ng-zorro-antd/i18n';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzSelectModule } from 'ng-zorro-antd/select';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NzButtonModule,
    NzGridModule,
    NzFormModule,
    NzNotificationModule,
    NzIconModule,
    NzModalModule,
    NzTableModule,
    NzGridModule,
    NzButtonModule,
    NzSpinModule,
    NzDropDownModule,
    NzDividerModule,
    NzLayoutModule,
    NzToolTipModule,
    NzSelectModule
  ],
  exports: [
    NzButtonModule,
    NzGridModule,
    NzFormModule,
    NzNotificationModule,
    NzIconModule,
    NzModalModule,
    NzTableModule,
    NzGridModule,
    NzButtonModule,
    NzSpinModule,
    NzDropDownModule,
    NzDividerModule,
    NzLayoutModule,
    NzToolTipModule,
    NzSelectModule
  ],
  providers: [
    { provide: NZ_I18N, useValue: es_ES }
  ],
})
export class NgZorroModule { }
