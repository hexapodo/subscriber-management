import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NzTableComponent } from 'ng-zorro-antd/table';
import { Subject, takeUntil } from 'rxjs';
import { IDatumC } from 'src/app/core/model/interface';

@Component({
  selector: 'app-table-countries',
  templateUrl: './table-countries.component.html',
  styleUrls: ['./table-countries.component.scss']
})
export class TableCountriesComponent implements OnInit {
  // propiedades de la tabla
  @ViewChild('virtualTable', { static: false }) nzTableComponent?: NzTableComponent<IDatumC>;
  private destroy$ = new Subject();
  // entrada de propiedad de paises
  @Input() countries: IDatumC[] = [];
  listCountries: IDatumC[] = [];

  // buscador interno de la tabla
  searchValue: any = '';
  visible = false;

  constructor() { }

  ngOnInit(): void {
    // 
    this.listCountries = [...this.countries];
    console.log('listCountries', this.listCountries);
  }
  // propiedades de la tabla
  ngAfterViewInit(): void {
    this.nzTableComponent?.cdkVirtualScrollViewport?.scrolledIndexChange
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: number) => {
        console.log('scroll index to', data);
      });
  }
  ngOnDestroy(): void {
    this.destroy$.complete();
  }
  // propiedades de la tabla
  scrollToIndex(index: number): void {
    this.nzTableComponent?.cdkVirtualScrollViewport?.scrollToIndex(index);
  }
  trackByIndex(_: number, data: IDatumC): number {
    return parseInt(data.Name);
  }
  /* funciones buscador de la tabla */
  reset(): void {
    this.searchValue = '';
    this.search();
  };
  search(): void {
    this.visible = false;
    this.listCountries = this.countries.filter((item: IDatumC) => item.Name.indexOf(this.searchValue) !== -1);
  };
}
