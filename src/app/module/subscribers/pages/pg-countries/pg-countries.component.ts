import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IDatumC } from 'src/app/core/model/interface';
import { AuthService } from 'src/app/module/auth/service/auth.service';
import { CountriesService } from '../../service/countries.service';

@Component({
  selector: 'app-pg-countries',
  templateUrl: './pg-countries.component.html',
  styleUrls: ['./pg-countries.component.scss']
})
export class PgCountriesComponent implements OnInit {

  countries: IDatumC[] = [];
    // @ts-ignore
  loading: boolean;
  
  constructor(
    private countriesService: CountriesService,
    private router: Router,
    private authService: AuthService
    ) { 
      this.authService.setLogout(true)
    }

  ngOnInit(): void {
    this.getListCountries();
  }
  getListCountries() {
    this.loading = true;
    this.countriesService.getListCountries().
      subscribe((resp) => {
        this.countries = resp?.Data;
        this.loading = false;
        console.log('countries', this.countries);
      })
  }
  onAtras(){
    this.router.navigate(['subscribers'])
  }

}
