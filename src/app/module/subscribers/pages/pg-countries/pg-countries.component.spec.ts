import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PgCountriesComponent } from './pg-countries.component';

describe('PgCountriesComponent', () => {
  let component: PgCountriesComponent;
  let fixture: ComponentFixture<PgCountriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PgCountriesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PgCountriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
