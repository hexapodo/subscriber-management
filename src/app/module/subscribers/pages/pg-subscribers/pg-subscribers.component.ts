import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { forkJoin } from 'rxjs';
import { IDatum } from 'src/app/core/model/interface';
import { AuthService } from 'src/app/module/auth/service/auth.service';
import { MessageService } from 'src/app/shared/service/message.service';
import { SubscribersService } from '../../service/subscribers.service';
import { FormSubscriberComponent } from './form-subscriber/form-subscriber.component';
import { ModalDetailSubscriberComponent } from './modal-detail-subscriber/modal-detail-subscriber.component';

@Component({
  selector: 'app-pg-subscribers',
  templateUrl: './pg-subscribers.component.html',
  styleUrls: ['./pg-subscribers.component.scss']
})
export class PgSubscribersComponent implements OnInit  {
  // @ts-ignore
  subscribers: IDatum[] = [];
  // @ts-ignore
  loading: boolean;
  validatedData: any;
  // @ts-ignore
  subscriber: IDatum;
  habilitarId: boolean = false;
  // @ts-ignore
  subscriberSubscription: Subscription;

  constructor(
    private subscribersService: SubscribersService,
    private authService: AuthService,
    private message: MessageService,
    private router: Router,
    private readonly modal: NzModalService,
  ) { 
    this.authService.setLogout(true)
  }
  
  ngOnInit(): void {
    this.getListSubscribers();
  }
  // obtener la lista de todos los suscriptores
  getListSubscribers(params: any = null) {
    this.loading = true;
    console.log('getListSubscribers', params);
    const initialParams = {
      page: 1,
      count: 10
    }
    this.subscriberSubscription = this.subscribersService.getListSubscribers(params !== null ? params : initialParams).
      subscribe((resp) => {
        this.subscribers = resp?.Data;
        console.log('subscribers', this.subscribers);
        this.loading = false;
        // cancelacion de la suscripción
        this.subscriberSubscription.unsubscribe();
      }, (error) => {
        console.log(error);
        this.loading = false;
      })
  }

  //llamada del servicio que trae el detalle del suscriptor
  detailSubscriber(item: any) {
    console.log(item.Id);
    this.subscribersService.getSubscriber(item.Id).subscribe(
      (resp) => {
        this.subscriber = resp;
        console.log('detailSubscriber', resp);
        this.modalDetailSubscriber();
      },
      (error) => {
        console.log('error', error);
      }
    )
  }
  //creación del modal para ver el detalle del suscriptor
  modalDetailSubscriber() {
    const modal: NzModalRef = this.modal.create({
      nzTitle: 'Detalle del subcriptor',
      nzContent: ModalDetailSubscriberComponent,
      nzComponentParams: {
        data: this.subscriber,
      },
      nzWidth: '30%',
      nzMaskClosable: false,
      nzKeyboard: false,
      nzFooter: [
        {
          label: 'Cerrar',
          onClick: () => modal.destroy()
        }
      ]
    });
  }
  /* modal para crear y editar Subscribers */
  onModalSubscribers(item: any) {
    console.log(item);
    let titulo: any = '';
    if (item === 'Crear') {
      titulo = item
      this.habilitarId = true
    } else {
      titulo = item.title
      this.habilitarId = false
    }
    const modal: NzModalRef = this.modal.create({
      nzTitle: titulo + ' subscriptor ',
      nzContent: FormSubscriberComponent,
      nzComponentParams: {
        data: item.subscriber,
        habilitarId: this.habilitarId
      },
      nzWidth: '50%',
      nzMaskClosable: false,
      nzKeyboard: false,
      nzOnOk: (valor: any) => {
        this.validatedData = valor.validateFormEnviar.value;
        console.log('datos del formulario ', this.validatedData)
        console.log('this.validatedData[0].Id ', this.validatedData.Subscribers[0]?.id)
        if (this.validatedData) {
          if (this.validatedData.Subscribers[0]?.id) {
            // API de modify
            this.subscribersService
              .updateSubscriber(this.validatedData.Subscribers[0]?.id, this.validatedData.Subscribers[0])
              .subscribe(
                (success) => {
                  console.log('se agrego actualizo registro', success);
                  this.message.showSuccess("Actualizado exitosamente", "Suscriptor");
                  this.getListSubscribers();
                },
                (error) => {
                  this.message.showError('Actualización Fallida', "Suscriptor");
                }
              );
          } else {
            console.log('nuevo', this.validatedData);
            // API de add
            this.subscribersService.addSubscriber(this.validatedData).subscribe(
              (success) => {
                console.log('se agrego nuevo registro', success);
                
                this.message.showSuccess("Registro exitosamente", "Suscriptor");
                this.getListSubscribers();
              },
              (error) => {
                this.message.showError("Registro fallido", "Suscriptor");
              }
            );
          }
        } else {
        }
      },
      nzOkDisabled: true,
    });
  }
  // funcion para eliminar un suscriptor
  onEliminar(item: any) {
    console.log(item.Id);
    this.modal.confirm({
      nzTitle: 'Eliminar suscriptor',
      nzContent: `Se va a eliminar <b style="color: red;">${item.Name}</b>, está seguro? `,
      nzOkText: 'Eliminar',
      nzOkType: 'primary',
      nzOkDanger: true,
      nzWidth: '30%',
      nzOnOk: () => {
        this.subscribersService.deleteSubscriber(item.Id).subscribe(
          (success) => {
            this.message.showSuccess("Eliminado exitosamente", "Suscriptor");
            this.getListSubscribers();
          },
          (error) => {
            this.message.showError("Error al eliminar", "Suscriptor");
          }
        );
      },
      nzCancelText: 'No',
    });
  }
  // ir a página de paises
  onCountries(){
    this.router.navigate(['subscribers/countries'])
  }
}

