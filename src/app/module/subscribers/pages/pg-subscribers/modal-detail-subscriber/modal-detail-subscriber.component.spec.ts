import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDetailSubscriberComponent } from './modal-detail-subscriber.component';

describe('ModalDetailSubscriberComponent', () => {
  let component: ModalDetailSubscriberComponent;
  let fixture: ComponentFixture<ModalDetailSubscriberComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalDetailSubscriberComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDetailSubscriberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
