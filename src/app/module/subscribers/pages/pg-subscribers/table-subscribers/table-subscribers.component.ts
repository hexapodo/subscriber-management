import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NzTableComponent } from 'ng-zorro-antd/table';
import { Subject, takeUntil } from 'rxjs';
import { IDatum, ISubscribers } from 'src/app/core/model/interface';
import { SubscribersService } from '../../../service/subscribers.service';

@Component({
  selector: 'app-table-subscribers',
  templateUrl: './table-subscribers.component.html',
  styleUrls: ['./table-subscribers.component.scss']
})
export class TableSubscribersComponent implements OnInit, OnChanges {
  // lo que emite la tabla
  @Output() editar: EventEmitter<any> = new EventEmitter();
  @Output() eliminar: EventEmitter<any> = new EventEmitter();
  @Output() detail: EventEmitter<any> = new EventEmitter();
  // propiedades de la tabla
  @ViewChild('virtualTable', { static: false }) nzTableComponent?: NzTableComponent<IDatum>;
  private destroy$ = new Subject();
  // propiedad de entrada
  @Input() subscribers: IDatum[] = [];
  listSubscribers: IDatum[] = [];

  // buscador interno de la tabla
  searchValue: any = '';
  visible = false;

  ///////////////////////////////////////////////////////
  pageIndex = 1;
  pageSize = 10;
  total = 20;
  //////////////////////////////////////////////////////
  constructor(private subscribersService: SubscribersService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    // con el fin de que se refresque la vista cuando hay cambios
    this.listSubscribers = [...this.subscribers];
    console.log('listSubscribers', this.listSubscribers);
  }

  ngOnInit(): void {
    // datos de inicio
    this.listSubscribers = [...this.subscribers];
    console.log('listSubscribers', this.listSubscribers);
    /* this.searchData(); */
  }
  // propiedades de la tabla
  ngAfterViewInit(): void {
    this.nzTableComponent?.cdkVirtualScrollViewport?.scrolledIndexChange
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: number) => {
        console.log('scroll index to', data);
      });
  }
  // propiedades de la tabla
  scrollToIndex(index: number): void {
    this.nzTableComponent?.cdkVirtualScrollViewport?.scrollToIndex(index);
  }
  trackByIndex(_: number, data: IDatum): number {
    return parseInt(data.Name);
  }

  // editar Subscriber
  onEditar(title: any, subscriber: ISubscribers) {
    console.log(title, subscriber);
    this.editar.emit({ title, subscriber })
  }
  // eliminar Subscriber
  onEliminar(subscriber: any) {
    console.log(subscriber);
    this.eliminar.emit(subscriber)
  }
  onDetail(subscriber: ISubscribers) {
    console.log(subscriber);
    this.detail.emit(subscriber)
  }
  /* funciones buscador de la tabla */
  reset(): void {
    this.searchValue = '';
    this.search();
  };
  search(): void {
    this.visible = false;
    this.listSubscribers = this.subscribers.filter((item: IDatum) => item.Name.indexOf(this.searchValue) !== -1);

  };

  /*  */
  searchData(reset: boolean = false): void {
    console.log('reset',reset);
    
    console.log('searchData');
    if (reset) {
      console.log('if',reset);
      this.pageIndex = 1;
    }
    console.log('this.pageIndex,',this.pageIndex,);
    console.log('this.pageSize',this.pageSize);
    const params = {
      page: this.pageIndex,
      count: this.pageSize
    }
    this.subscribersService.getListSubscribers(params).
      subscribe((resp) => {
        this.total = resp?.Count;
        this.listSubscribers = resp?.Data;
        console.log('subscribers', this.subscribers);
      }, (error) => {
        console.error(error);
      })
    // this.subscribersService
    //   .getUsers(this.pageIndex, this.pageSize,)
    //   .subscribe((data: any) => {
    //     console.log('data',data);
    //     this.total = 20;
    //     this.listSubscribers = data.results;
    //     console.log('listSubscribers', this.listSubscribers);
    //   });
  }
}