import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BackendServiceInterface, ICountries} from 'src/app/core/model/interface';
import { BackendConnection } from 'src/app/shared/service/backend-connection.service';
import { StorageManagerService } from 'src/app/shared/service/storage-manager.service';
import { AuthService } from '../../auth/service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class CountriesService extends BackendConnection implements BackendServiceInterface {
  endpoint = 'countries';

  constructor(
    protected override http: HttpClient,
    protected override storageManagerService: StorageManagerService,
    protected override authService: AuthService

  ) {
    super(http, storageManagerService, authService);
  }
  getListCountries(): Observable<ICountries> {
    return super.getAll(this.endpoint);
  }

}
