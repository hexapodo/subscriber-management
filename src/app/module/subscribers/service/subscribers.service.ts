import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { BackendServiceInterface, IDatum, ISubscribers } from 'src/app/core/model/interface';
import { BackendConnection } from 'src/app/shared/service/backend-connection.service';
import { StorageManagerService } from 'src/app/shared/service/storage-manager.service';
import { environment } from 'src/environments/environment';
import { AuthService } from '../../auth/service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class SubscribersService extends BackendConnection implements BackendServiceInterface {
  endpoint = 'subscribers';
  ruta = environment.backendUrl;
  constructor(
    protected override http: HttpClient,
    protected override storageManagerService: StorageManagerService,
    protected override authService: AuthService

  ) {
    super(http, storageManagerService, authService);
  }
  getListSubscribers(params: any = null): Observable<ISubscribers> {
    return super.getAll(this.endpoint, params);
  }
  getSubscriber(id: any): Observable<IDatum> {
    return super.getOne(this.endpoint, id);
  }
  addSubscriber(data: any): Observable<any> {
    return super.post(this.endpoint, data);
  }
  updateSubscriber(id: any, data: any): Observable<any> {
    console.log(id, data);
    return super.put(this.endpoint, id, data);
  }
  deleteSubscriber(id: any): Observable<any> {
    return super.delete(this.endpoint, id);
  }

  /* ?page=2&count=2 */
  
  getUsers(
    pageIndex: number = 2,
    pageSize: number = 3,
  ): Observable<{}> {
    let params = new HttpParams()
      .append('page', `${pageIndex}`)
      .append('count', `${pageSize}`)
      console.log(this.endpoint);
    return this.http.get(`${this.ruta}${this.endpoint}/`, {
      params
    });
    
  }
}
