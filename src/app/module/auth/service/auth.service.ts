import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Route, Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { ILogin } from 'src/app/core/model/interface';
import { MessageService } from 'src/app/shared/service/message.service';
import { StorageManagerService } from 'src/app/shared/service/storage-manager.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  endpoint = 'account/login'
  url: string;

  private unlogged$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true)

  constructor(
    protected http: HttpClient,
    private storageManagerService: StorageManagerService,
    private message: MessageService,
    private router: Router
  ) {
    this.url = environment.backendUrl;
  }
  get unlogged(): Observable<boolean> {
    return this.unlogged$.asObservable();
  }
  setLogout(unlogged: boolean): void {
    this.unlogged$.next(unlogged);
  }
  login(credenciales: ILogin): Observable<any> {
    return this.http.post(this.url + this.endpoint, credenciales)
  }
  logout(expirado = false) {
    this.storageManagerService.destroy('token');
    if (expirado) {
      this.message.showInfo('Expirado,debes volver a iniciar sesión', "Token");
      this.router.navigate(['login'])
    }
  }

}
