import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SessionGuard } from './core/guards/session.guard';
import { AuthRoutingModule } from './module/auth/auth-routing.routing';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import(`./module/auth/auth.module`).then(m => m.AuthModule)
  },
  {
    path: 'subscribers',
    loadChildren: () => import(`./module/subscribers/subscribers.module`).then(m => m.SubscribersModule), canActivate: [SessionGuard]
  },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true }),
    AuthRoutingModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
