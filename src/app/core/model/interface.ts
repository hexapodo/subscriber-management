export interface BackendServiceInterface {
  url: string;
  endpoint: string;
}
export interface ILogin {
  userName: string;
  password: string
}
export interface ISubscribers {
  Count: number;
  Data: IDatum[];
}

export interface IDatum {
  SystemId: null;
  Area: string;
  PublicId: number;
  CountryCode: string;
  CountryName: string;
  Name: string;
  EndpointsCount: number;
  Email: string;
  JobTitle: string;
  PhoneNumber: string;
  PhoneCode: string;
  PhoneCodeAndNumber: string;
  LastActivityUtc: null;
  LastActivity: null;
  LastActivityString: null;
  SubscriptionDate: null;
  SubscriptionMethod: number;
  SubscriptionState: number;
  SubscriptionStateDescription: string;
  Topics: any[];
  ValidEmail: boolean;
  Activity: string;
  ConnectionState: number;
  Id: number;
}

export interface ICountries {
  Count: number;
  Data:  IDatumC[];
}

export interface IDatumC {
  Code:      string;
  Code3:     null | string;
  Name:      string;
  PhoneCode: null | string;
}
